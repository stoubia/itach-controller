import socket
import mmap
import Find_iTach
import sys

ITACH_IP = '192.168.0.106'
ITACH_PORT = 4998

IR_COMMANDS = 'D:\Google Drive\Projects\iTach\iTach\iTach\LearnedIR.txt'


def main(argv):
    if len(argv) > 1:
        command = argv[1]
        print 'Command:', command

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print 'connecting socket', (ITACH_IP,ITACH_PORT)
        s.connect((ITACH_IP, ITACH_PORT))
        print 'sending command:', command
        
        IR = get_IR(command)
        if IR != -1:
            s.send(IR)
            print 'Response:', repr(s.recv(1024))
            print 'closing socket'
            s.close()
        else:
            print 'IR command not found'
    else:
        print 'No command given'

#searches IR commands text file for command and returns a formatted IR command string to be 
#sent to the iTach device
#this can potentially provide false-positives if the provided command is a partial name of an existing command
#example: command == 'HDMI', this will cause the first line containing 'HDMI' to be returned, such as any of the HDMI switch input commands
def get_IR(command):
    print 'Getting Command from txt file'
    with open(IR_COMMANDS, 'r+b') as f:
        mm = mmap.mmap(f.fileno(), 0)
        index = mm.find(command)
        if index >= 0:
            mm.seek(index)
            return format_IR(mm.readline())
        else:
            return -1

#input: a readline() value of the IR commands text file
#returns: only the IR command, removing the key word and '\n'
def format_IR(str):
    return str[str.find('sendir'):].replace('\n','')

main(sys.argv)