import socket
import struct
import sys

 #listens for iTach UDP multicast from 239.255.250.250 port 9131
MULTICAST_IP = '239.255.250.250'
PORT = 9131
BEACON = (MULTICAST_IP, PORT)

def Find_iTach():
   sb = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
   sb.bind(('', PORT))
   group = socket.inet_aton(MULTICAST_IP)
   mreq = struct.pack('4sL', group, socket.INADDR_ANY)
   sb.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
   
   print >>sys.stderr, 'Searching...'
   data, address = sb.recvfrom(1024)
   print >>sys.stderr, '\niTach device found on', address
   sb.close()


